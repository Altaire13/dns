#include <iostream>
#include <cmath>
#include <cassert>
#include <memory>
#include <cstddef>
#include <string>
#include <fstream>
#include <stdexcept>

#include <fcntl.h>
#include <unistd.h>
#include <cstring>

#include <mpi.h>

using std::string;

struct MatConf {
    MatConf(char type = 'd', int64_t n = 0, int64_t m = 0) : type(type), n(n), m(m) {}

    char type;
    int64_t n;
    int64_t m;
};

struct MatInfo {
    MatInfo(const MatConf& conf, int dim_proc_ext) {
        n = static_cast<int>(conf.n);
        m = static_cast<int>(conf.m);

        mat_block_ext = (n + dim_proc_ext - 1) / dim_proc_ext;
        mat_block_size = mat_block_ext * mat_block_ext;
    }

    MatConf GetConf() const {
        return MatConf('d', n, m);
    }

    int n;
    int m;
    int mat_block_ext;
    int mat_block_size;
};

class DMatrix {
public:
    DMatrix(const MatInfo& info)
            : info(info), loc_n(info.mat_block_ext), loc_m(info.mat_block_ext) {
        data = new double[loc_m * loc_n]();
    }

    virtual ~DMatrix() {
        delete[] data;
    }

    double& operator ()(int row, int col) {
        return data[row * loc_m + col];
    }

    double* get_data() {
        return data;
    }

    const MatInfo info;
    const int loc_n;
    const int loc_m;

private:
    double* data;
};

struct GridInfo {
    GridInfo(int x, int y, int z, const GridInfo& other) : x(x), y(y), z(z), dim_proc_ext(other.dim_proc_ext),
                                                           rank(recalc_rank()) {
    }

    GridInfo(int rank, int dim) : dim_proc_ext(dim), rank(rank) {
        y = rank % dim;
        x = rank / dim % dim;
        z = rank / (dim * dim);
    }

    bool operator ==(const GridInfo& other) const {
        return x == other.x && y == other.y && z == other.z;
    }

    GridInfo diagonal(int dim) const {
        switch (dim) {
            case 0:
                return GridInfo(x, y, y, *this);
            case 1:
                return GridInfo(x, y, x, *this);
            default:
                throw std::runtime_error("Wrong diagonal dimension");
        }
    }

    bool isDiagonal(int dim) const {
        switch (dim) {
            case 0:
                return y == z;
            case 1:
                return x == z;
            default:
                throw std::runtime_error("Wrong diagonal dimension");
        }
    }

    int recalc_rank() {
        return z * dim_proc_ext * dim_proc_ext + x * dim_proc_ext + y;
    }

    GridInfo flat() const {
        return GridInfo(x, y, 0, *this);
    }

    GridInfo projectFace(int dim) const {
        switch (dim) {
            case 0:
                return GridInfo(x, z, 0, *this);
            case 1:
                return GridInfo(y, z, 0, *this);
            default:
                throw std::runtime_error("Wrong projection");
        }
    }

    int x;
    int y;
    int z;

    int dim_proc_ext;
    int rank;
};

MatConf ReadMatConfCheck(int rank, const string& mat_filename);

void
ReadMatrix(const GridInfo& grid_info, MPI_Comm reader_comm, const string& mat_filename, DMatrix* target);

void
WriteMatrix(const GridInfo& grid_info, const string& mat_filename, DMatrix* source);

void
WriteMatrixSystem(const GridInfo& grid_info, const string& mat_filename, DMatrix* source);

void
DistributeMatrix3D(int rank, const GridInfo& grid_pos, bool init_layer_flag, DMatrix* init_matrix,
                   int diagonal_index, DMatrix* target);

DMatrix*
ReadAndDistributeMatrix(const GridInfo& grid_info, const string& mat_filename, int diagonal_index);

void OutputBlock(const GridInfo& grid_info, DMatrix* mat, const GridInfo& target_block);

DMatrix* MultiplyDNS(const GridInfo& grid_info, DMatrix* mat_a, DMatrix* mat_b);

MPI_Datatype CreateMatConfType();

MPI_Datatype CreateMatDataType(const GridInfo& grid_info, const DMatrix* target);

double end_init;
double end_input_mat[2];
double end_distr_mat[2];
double end_calc;
double end_output;

void check_time(int rank, double& checkpoint) {
    if (rank == 0) {
        checkpoint = MPI_Wtime();
    }
}

int main(int argc, char** argv) {
    MPI_Init(&argc, &argv);

    int rank;
    int size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    if (argc < 4) {
        if (rank == 0) {
            std::cout << "Usage: " << argv[0] << " mat_a_file mat_b_file mat_res_file [s]" << std::endl;
        }

        MPI_Finalize();

        return 0;
    }
    
    string mat_a_filename = argv[1];
    string mat_b_filename = argv[2];
    string mat_c_filename = argv[3];

    bool serial_output = false;

    if (argc > 4) {
        if (argv[4][0] == 's') {
            serial_output = true;
        }
    }

    int mpi_ull_size;
    MPI_Type_size(MPI_UNSIGNED_LONG_LONG, &mpi_ull_size);
    assert(mpi_ull_size == sizeof(uint64_t));

    // Extension of process grid over each dimension
    int dim_proc_ext = static_cast<int>(cbrt(size));

    assert(dim_proc_ext * dim_proc_ext * dim_proc_ext == size);

    if (rank == 0) {
        std::cout << "Process grid: " << dim_proc_ext << " x " << dim_proc_ext << " x " << dim_proc_ext << std::endl;
    }

    GridInfo grid_info(rank, dim_proc_ext);

    check_time(rank, end_init);

    DMatrix* mat_a = ReadAndDistributeMatrix(grid_info, mat_a_filename, 0);
    check_time(rank, end_distr_mat[0]);

    DMatrix* mat_b = ReadAndDistributeMatrix(grid_info, mat_b_filename, 1);
    check_time(rank, end_distr_mat[1]);

    DMatrix* mat_c = MultiplyDNS(grid_info, mat_a, mat_b);
    check_time(rank, end_calc);

    delete mat_a;
    delete mat_b;

    if (serial_output) {
        WriteMatrixSystem(grid_info, mat_c_filename, mat_c);
    } else {
        WriteMatrix(grid_info, mat_c_filename, mat_c);
    }

    check_time(rank, end_output);

    if (rank == 0) {
        double input_time = end_input_mat[0] - end_init + end_input_mat[1] - end_distr_mat[0];
        double calc_time = end_distr_mat[0] - end_input_mat[0] + end_calc - end_input_mat[1];
        double output_time = end_output - end_calc;

        std::cout << "Input:  " << input_time << std::endl;
        std::cout << "Algo:   " << calc_time << std::endl;
        std::cout << "Output: " << output_time << std::endl;

        FILE* log = fopen("log.out", "a");
        fprintf(log, "%d,%d,%lf,%lf,%lf,%s\n", grid_info.dim_proc_ext, mat_c->info.n, input_time, calc_time,
                output_time, serial_output ? "serial" : "mpi");
    }

    delete mat_c;

    if (grid_info.rank == 0) {
        std::cout << "Finalizing" << std::endl;
    }

    MPI_Finalize();

    return 0;
}

DMatrix* MultiplyDNS(const GridInfo& grid_info, DMatrix* mat_a, DMatrix* mat_b) {
    DMatrix* mat_c = new DMatrix(mat_a->info);

    for (int i = 0; i < mat_a->loc_n; ++i) {
        for (int k = 0; k < mat_a->loc_m; ++k) {
            for (int j = 0; j < mat_b->loc_m; ++j) {
                (*mat_c)(i, j) += (*mat_a)(i, k) * (*mat_b)(k, j);
            }
        }
    }

    DMatrix* result = new DMatrix(mat_c->info);

    MPI_Comm reduce;
    MPI_Comm_split(MPI_COMM_WORLD, grid_info.flat().rank, grid_info.rank, &reduce);
    MPI_Reduce(mat_c->get_data(), result->get_data(), mat_c->info.mat_block_size, MPI_DOUBLE, MPI_SUM, 0, reduce);

    delete mat_c;

    return result;
}

void OutputBlock(const GridInfo& grid_info, DMatrix* mat, const GridInfo& target_block) {
    if (grid_info == target_block) {
        for (int i = 0; i < mat->info.mat_block_ext; ++i) {
            for (int j = 0; j < mat->info.mat_block_ext; ++j) {
                std::cout << (*mat)(i, j) << " ";
            }
            std::cout << std::endl;
        }
    }
}

DMatrix*
ReadAndDistributeMatrix(const GridInfo& grid_info, const string& mat_filename, int diagonal_index) {
    MPI_Comm comm_a_layer;
    MPI_Comm_split(MPI_COMM_WORLD, grid_info.z, grid_info.rank, &comm_a_layer);
    MatConf mat_conf = ReadMatConfCheck(grid_info.rank, mat_filename);
    MatInfo mat_info = MatInfo(mat_conf, grid_info.dim_proc_ext);

    if (grid_info.rank == 0) {
        std::cout << "Block size: " << mat_info.mat_block_ext << std::endl;
        std::cout << "Begin reading matrix" << std::endl;
    }

    bool init_a_layer = grid_info.rank < grid_info.dim_proc_ext * grid_info.dim_proc_ext;
    DMatrix* init_mat_a = new DMatrix(mat_info);
    if (init_a_layer) {
        ReadMatrix(grid_info, comm_a_layer, mat_filename, init_mat_a);
    }

    if (grid_info.rank == 0) {
        std::cout << "Read completed" << std::endl;
    }

    check_time(grid_info.rank, end_input_mat[diagonal_index]);

    DMatrix* mat_a = new DMatrix(mat_info);
    DistributeMatrix3D(grid_info.rank, grid_info, init_a_layer, init_mat_a, diagonal_index, mat_a);

    delete init_mat_a;

    return mat_a;
}

void
DistributeMatrix3D(int rank, const GridInfo& grid_pos, bool init_layer_flag, DMatrix* init_matrix,
                   int diagonal_index, DMatrix* target) {
    if (rank == 0) {
        std::cout << "Distributing matrix" << std::endl;
    }

    MPI_Request request = MPI_REQUEST_NULL;

    if (init_layer_flag) {
        MPI_Isend(init_matrix->get_data(), init_matrix->info.mat_block_size, MPI_DOUBLE,
                  grid_pos.diagonal(diagonal_index).rank, 0, MPI_COMM_WORLD, &request);
    }

    if (rank == 0) {
        std::cout << "Sent mat A" << std::endl;
    }

    if (grid_pos.isDiagonal(diagonal_index)) {
        MPI_Status status;
        MPI_Recv(target->get_data(), target->info.mat_block_size, MPI_DOUBLE, grid_pos.flat().rank,
                 MPI_ANY_TAG,
                 MPI_COMM_WORLD, &status);
    }

    if (init_layer_flag) {
        MPI_Wait(&request, MPI_STATUS_IGNORE);
    }

    if (rank == 0) {
        std::cout << "Recieved mat" << std::endl;
    }

    MPI_Comm comm_a_broad;
    MPI_Comm_split(MPI_COMM_WORLD, grid_pos.projectFace(diagonal_index).rank, rank, &comm_a_broad);

    MPI_Bcast(target->get_data(), target->info.mat_block_size, MPI_DOUBLE, grid_pos.z, comm_a_broad);
}

void
ReadMatrix(const GridInfo& grid_info, MPI_Comm reader_comm, const string& mat_filename, DMatrix* target) {
    MPI_Datatype MatDataType = CreateMatDataType(grid_info, target);

    MPI_File mat_file;
    MPI_File_open(reader_comm, const_cast<char*>(mat_filename.c_str()), MPI_MODE_RDONLY, MPI_INFO_NULL, &mat_file);
    MPI_File_set_view(mat_file, sizeof(MatConf), MPI_DOUBLE, MatDataType, "native", MPI_INFO_NULL);

    if (grid_info.rank == 0) {
        std::cout << "Actually read" << std::endl;
    }

    MPI_File_read_all(mat_file, target->get_data(), target->info.mat_block_size, MPI_DOUBLE, MPI_STATUS_IGNORE);
    MPI_File_close(&mat_file);
}

MPI_Datatype CreateMatDataType(const GridInfo& grid_info, const DMatrix* target) {
    int gsizes[2] = {target->info.n, target->info.m};
    int distribs[2] = {MPI_DISTRIBUTE_BLOCK, MPI_DISTRIBUTE_BLOCK};
    int dargs[2] = {MPI_DISTRIBUTE_DFLT_DARG, MPI_DISTRIBUTE_DFLT_DARG};
    int psizes[2] = {grid_info.dim_proc_ext, grid_info.dim_proc_ext};
    MPI_Datatype MatDataType;
    MPI_Type_create_darray(grid_info.dim_proc_ext * grid_info.dim_proc_ext, grid_info.rank, 2, gsizes, distribs,
                           dargs, psizes, MPI_ORDER_C, MPI_DOUBLE, &MatDataType);
    MPI_Type_commit(&MatDataType);

    return MatDataType;
}

MatConf ReadMatConfCheck(int rank, const string& mat_filename) {
    MatConf mat_conf;
    int mpi_err;

    MPI_File mat_file;
    mpi_err = MPI_File_open(MPI_COMM_WORLD, const_cast<char*>(mat_filename.c_str()), MPI_MODE_RDONLY, MPI_INFO_NULL,
                            &mat_file);

    if (rank == 0 && mpi_err != MPI_SUCCESS) {
        std::cout << "File open error" << std::endl;
        std::cout << "MPI error code: " << mpi_err << std::endl;
        char err_str[MPI_MAX_ERROR_STRING];
        int err_len;
        MPI_Error_string(mpi_err, err_str, &err_len);
        std::cout << "Error string: " << err_str << std::endl;
    }

    MPI_Datatype MatConfType = CreateMatConfType();

    MPI_File_set_view(mat_file, 0, MatConfType, MatConfType, "native", MPI_INFO_NULL);
    MPI_File_read_all(mat_file, &mat_conf, 1, MatConfType, MPI_STATUS_IGNORE);

    MPI_File_close(&mat_file);

    if (rank == 0) {
        std::cout << "type: " << mat_conf.type << " (" << (int) mat_conf.type << ")" << std::endl;
        std::cout << "N x M: " << std::hex << mat_conf.n << " x " << mat_conf.m << std::dec << std::endl;
    }

    assert(mat_conf.n == mat_conf.m);

    return mat_conf;
}

MPI_Datatype CreateMatConfType() {
    static const int MAT_CONF_COUNT = 3;

    int block_lengths[MAT_CONF_COUNT] = {1, 1, 1};
    MPI_Aint displs[MAT_CONF_COUNT] = {offsetof(MatConf, type), offsetof(MatConf, n), offsetof(MatConf, m)};
    MPI_Datatype types[MAT_CONF_COUNT] = {MPI_CHAR, MPI_UNSIGNED_LONG_LONG, MPI_UNSIGNED_LONG_LONG};

    MPI_Datatype MatConfType;
    MPI_Type_create_struct(MAT_CONF_COUNT, block_lengths, displs, types, &MatConfType);
    MPI_Type_commit(&MatConfType);
    
    return MatConfType;
}

void
WriteMatrix(const GridInfo& grid_info, const string& mat_filename, DMatrix* source) {
    if (grid_info.rank == 0) {
        MPI_Datatype MatConfType = CreateMatConfType();

        MPI_File mat_file;
        MPI_File_open(MPI_COMM_SELF, const_cast<char*>(mat_filename.c_str()), MPI_MODE_WRONLY | MPI_MODE_CREATE,
                      MPI_INFO_NULL, &mat_file);
        MPI_File_set_view(mat_file, 0, MatConfType, MatConfType, "native", MPI_INFO_NULL);

        MatConf mat_conf = source->info.GetConf();
        MPI_File_write(mat_file, &mat_conf, 1, MatConfType, MPI_STATUS_IGNORE);

        MPI_File_close(&mat_file);
    }

    MPI_Comm writer_comm;

    if (grid_info.rank < grid_info.dim_proc_ext * grid_info.dim_proc_ext) {
        MPI_Comm_split(MPI_COMM_WORLD, 0, grid_info.rank, &writer_comm);

        MPI_Datatype MatDataType = CreateMatDataType(grid_info, source);

        MPI_File mat_file;
        MPI_File_open(writer_comm, const_cast<char*>(mat_filename.c_str()), MPI_MODE_WRONLY, MPI_INFO_NULL, &mat_file);
        MPI_File_set_view(mat_file, sizeof(MatConf), MPI_DOUBLE, MatDataType, "native", MPI_INFO_NULL);

        MPI_File_write_all(mat_file, source->get_data(), source->info.mat_block_size, MPI_DOUBLE, MPI_STATUS_IGNORE);
        MPI_File_close(&mat_file);
    } else {
        MPI_Comm_split(MPI_COMM_WORLD, MPI_UNDEFINED, grid_info.rank, &writer_comm);
    }
}

void WriteMatrixSystem(const GridInfo& grid_info, const string& mat_filename, DMatrix* source) {
    MPI_Comm writer_comm;

    if (grid_info.rank < grid_info.dim_proc_ext * grid_info.dim_proc_ext) {
        MPI_Comm_split(MPI_COMM_WORLD, 0, grid_info.rank, &writer_comm);
    } else {
        MPI_Comm_split(MPI_COMM_WORLD, MPI_UNDEFINED, grid_info.rank, &writer_comm);
    }

    if (grid_info.rank == 0) {
        FILE* file = fopen(mat_filename.c_str(), "wb");

        MatConf mat_conf = source->info.GetConf();
        fwrite(&mat_conf, sizeof(mat_conf), 1, file);

        fclose(file);
    }

    if (grid_info.rank < grid_info.dim_proc_ext * grid_info.dim_proc_ext) {
        if (grid_info.rank != 0) {
            MPI_Recv(NULL, 0, MPI_BYTE, grid_info.rank - 1, MPI_ANY_TAG, writer_comm, MPI_STATUS_IGNORE);
        }

        FILE* file = fopen(mat_filename.c_str(), "r+b");

        std::cout << "x, y: " << grid_info.x << ", " << grid_info.y << std::endl;
        int head_elements = (grid_info.x * grid_info.dim_proc_ext * grid_info.dim_proc_ext + grid_info.y) *
                source->info.mat_block_ext;
        std::cout << "Head elements: " << head_elements << std::endl;
        int head = head_elements * sizeof(double);
        fseek(file, sizeof(MatConf) + head, SEEK_SET);

        int gap_elements = (grid_info.dim_proc_ext - 1) * source->info.mat_block_ext;
        std::cout << "Gap elements: " << gap_elements << std::endl;
        int gap = gap_elements * sizeof(double);

        for (int i = 0; i < source->loc_n; ++i) {
            fwrite(source->get_data() + i * source->loc_m, static_cast<size_t>(source->info.mat_block_ext),
                   sizeof(source->get_data()[0]),
                   file);
            fseek(file, gap, SEEK_CUR);
        }

        fclose(file);

        if (grid_info.rank != grid_info.dim_proc_ext * grid_info.dim_proc_ext - 1) {
            MPI_Send(NULL, 0, MPI_BYTE, grid_info.rank + 1, 0, writer_comm);
        }
    }

    MPI_Barrier(MPI_COMM_WORLD);
}
